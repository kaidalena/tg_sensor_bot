import configparser
import time
from subprocess import check_output

import telebot


emoji = {
    'hello': 128075,
    'sleep': 128164,
    'x': 10060,
    'o': 128683,
    'ok': 9989,
}


bot = telebot.TeleBot("token")
chat_id = 0
command_timeout = 3


@bot.message_handler(content_types=["text"])
def main(message):
    if message.chat.id == chat_id:
        command = message.text
        try:
            out = check_output(command, shell=True, timeout=command_timeout)
            bot.send_message(message.chat.id, out)
        except:
            bot.send_message(message.chat.id, f'{chr(emoji["o"])} Invalid command  <{command}>')


if __name__ == '__main__':
    bot.send_message(chat_id, f'Hello {chr(emoji["hello"])}')

    while True:
        try:
            bot.polling(none_stop=True)
        except:
            bot.send_message(chat_id, f'Sleep {chr(emoji["sleep"])}')
            time.sleep(10)
