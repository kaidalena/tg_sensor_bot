# Telegram sensor bot


## Install requirements

- Install python
- Install pip
- Install requirements with the command:

```
pip install -r requirements.txt
```

## Before start bot

In **_config.ini_** set your parameters
- TG_TOKEN - bot's token in telegram
- CHAT_ID - your user id in telegram
- TIMEOUT_SLEEP - time sleep, after bot failed
- TIMEOUT_COMMAND - time wait command execute

## Start bot
Execute command:
```commandline
python tgbot.py
```